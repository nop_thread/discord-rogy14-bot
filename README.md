# discord-rogy14-bot

[![Build Status](https://gitlab.com/nop_thread/discord-rogy14-bot/badges/develop/pipeline.svg)](https://gitlab.com/nop_thread/discord-rogy14-bot/pipelines/)
![Minimum supported rustc version: 1.56](https://img.shields.io/badge/rustc-1.56+-lightgray.svg)

# Bot Permissions

Currently required permisions are:

* Send Messages
* Embed Links
* Attach Files
* Read Message History

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

//! Datetime-related utilities.

use std::fmt;

use chrono::{DateTime, Duration, Utc};

/// Time delta.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct TimeDelta(Duration);

impl TimeDelta {
    /// Returns the time duration since now.
    #[must_use]
    pub fn since_now<Tz: chrono::TimeZone>(dt: DateTime<Tz>) -> Self {
        Self::from(dt.signed_duration_since(Utc::now()))
    }

    /// Returns printable object for time delta in minutes.
    #[must_use]
    pub fn display_human_readable(self) -> impl fmt::Display + 'static {
        /// Printable time delta.
        #[derive(Debug, Clone, Copy)]
        enum Display {
            /// Delta in days.
            Days(i64),
            /// Delta in hours.
            Hours(i64),
            /// Delta in minutes.
            Minutes(i64),
            /// Delta in seconds.
            Seconds(i64),
        }

        impl fmt::Display for Display {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                match *self {
                    Self::Days(d) if d > 0 => write!(f, "{}日後", d),
                    Self::Days(d) => write!(f, "{}日前", -d),
                    Self::Hours(d) if d > 0 => write!(f, "{}時間後", d),
                    Self::Hours(d) => write!(f, "{}時間前", -d),
                    Self::Minutes(d) if d > 0 => write!(f, "{}分後", d),
                    Self::Minutes(d) => write!(f, "{}分前", -d),
                    Self::Seconds(d) if d > 0 => write!(f, "{}秒後", d),
                    Self::Seconds(0) => f.write_str("今"),
                    Self::Seconds(d) => write!(f, "{}秒前", -d),
                }
            }
        }

        let delta = self.0;

        let days = delta.num_days();
        if days != 0 {
            return Display::Days(days);
        }

        let hours = delta.num_hours();
        if hours != 0 {
            return Display::Hours(hours);
        }

        let minutes = delta.num_minutes();
        if minutes != 0 {
            return Display::Minutes(minutes);
        }

        Display::Seconds(delta.num_seconds())
    }
}

impl From<Duration> for TimeDelta {
    #[inline]
    fn from(v: Duration) -> Self {
        Self(v)
    }
}

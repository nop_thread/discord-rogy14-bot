//! Discord bot for rogy14 server.

#![forbid(unsafe_code)]
//#![forbid(unsafe_op_in_unsafe_fn)]
//#![forbid(clippy::undocumented_unsafe_blocks)]
#![warn(rust_2018_idioms)]
// `clippy::missing_docs_in_private_items` implies `missing_docs`.
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::unwrap_used)]

mod among_us;
mod datetime;
mod general;

use std::collections::HashSet;

use serenity::framework::standard::macros::help;
use serenity::framework::standard::{
    help_commands, Args, CommandGroup, CommandResult, HelpOptions,
};
use serenity::framework::StandardFramework;
use serenity::model::channel::Message;
use serenity::model::id::UserId;
use serenity::prelude::*;
use serenity::{async_trait, Client};

use self::among_us::AmongUsState;

/// Entrypoint.
#[tokio::main]
async fn main() -> anyhow::Result<()> {
    init_logger();

    match dotenv::dotenv() {
        Ok(path) => log::info!("Loaded env file {}", path.display()),
        Err(e) => log::info!("Env file is not loaded: {}", e),
    }

    let token = std::env::var("DISCORD_TOKEN")?;

    let framework = StandardFramework::new()
        .configure(|c| c.prefix("!"))
        .help(&HELP)
        .group(&crate::general::GROUP)
        .group(&crate::among_us::GROUP);

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .framework(framework)
        .await?;

    {
        let mut data_lock = client.data.write().await;
        data_lock.insert::<AmongUsState>(AmongUsState::create_shared());
    }

    client.start().await?;

    Ok(())
}

/// Initialize the global logger.
fn init_logger() {
    /// Default log filter for debug build.
    #[cfg(debug_assertions)]
    const DEFAULT_LOG_FILTER: &str = "discord_rogy14_bot=debug";
    /// Default log filter for release build.
    #[cfg(not(debug_assertions))]
    const DEFAULT_LOG_FILTER: &str = "discord_rogy14_bot=warn";

    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or(DEFAULT_LOG_FILTER))
        .init();
}

/// Event handler.
struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: serenity::model::gateway::Ready) {
        log::info!("Bot user `{}` is connected and ready", ready.user.name);
    }

    async fn message(&self, ctx: Context, new_msg: Message) {
        // Check if the message seems like room code of "Among Us".
        if let Some(ev) = among_us::RoomCodeEvent::try_from_message(&new_msg) {
            if let Some(guild_id) = new_msg.guild_id {
                log::trace!(
                    "seems like Among Us room code: {:?} (in guild {:?})",
                    new_msg.content,
                    guild_id.name(&ctx).await
                );

                AmongUsState::set_event_code_through_context(&ctx, guild_id, Some(ev)).await;
            }
        }
    }
}

/// Hnadles help command.
#[help]
async fn help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = help_commands::with_embeds(context, msg, args, help_options, groups, owners).await;
    Ok(())
}

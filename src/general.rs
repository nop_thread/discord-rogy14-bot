//! General commands.

use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::CommandResult;
use serenity::model::prelude::*;
use serenity::prelude::*;

pub use GENERAL_GROUP as GROUP;

#[group]
#[prefixes("rogy14", "14")]
#[description = "General commands"]
#[summary = "General commands"]
#[commands(version, ping)]
#[default_command(unknown_command)]
pub struct General;

#[command]
#[description = "Show bot version"]
#[aliases("ver")]
pub async fn version(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id
        .say(&ctx.http, env!("CARGO_PKG_VERSION"))
        .await?;

    Ok(())
}

#[command]
#[description = "Ping the bot"]
#[aliases("御社", "まだ早い")]
pub async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    let cmd_name = msg.content.split_whitespace().nth(1);
    let pong = match cmd_name.unwrap_or("ping") {
        "ping" => "pong",
        "御社" => "感謝",
        "まだ早い" => "もう遅い",
        cmd_name => {
            let guild_name = match msg.guild_id {
                Some(id) => id.name(ctx).await,
                None => None,
            };
            log::error!(
                "Unknown alias to `ping` command: alias {:?} is used in guild {:?}",
                cmd_name,
                guild_name
            );
            "日本語でおｋ"
        }
    };
    msg.channel_id.say(&ctx.http, pong).await?;

    Ok(())
}

// もうこれが ping の代わりでよくない？
#[command]
pub async fn unknown_command(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(&ctx.http, "は？").await?;

    Ok(())
}

//! Commands for the game "Among Us".

pub mod config;

use std::collections::HashMap;
use std::fmt;
use std::sync::Arc;

use serenity::builder::{CreateEmbed, CreateMessage};
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::futures::StreamExt;
use serenity::model::id::{GuildId, MessageId};
use serenity::model::prelude::*;
use serenity::prelude::*;

use crate::datetime::TimeDelta;

use self::config::Config;

pub use AMONGUS_GROUP as GROUP;

/// A key type for the state for Among Us module.
#[derive(Default, Debug, Clone)]
pub struct AmongUsState {
    /// Room code events.
    codes: HashMap<GuildId, Option<RoomCodeEvent>>,
}

impl AmongUsState {
    /// Creates a new safely-sharable state.
    pub fn create_shared() -> <Self as TypeMapKey>::Value {
        Arc::new(RwLock::new(Self::default()))
    }

    /// Returns the latest event code for the guild if available.
    ///
    /// * `None`: Uninitialized.
    /// * `Some(None)`: Initialized and no room codes found.
    /// * `Some(Some(code))`: Initialized and room code event is cached.
    pub fn get_event_code(&self, guild_id: GuildId) -> Option<&Option<RoomCodeEvent>> {
        self.codes.get(&guild_id)
    }

    /// Retrieves the latest event code for the guild from the `Context`.
    pub async fn get_event_code_through_context(
        ctx: &Context,
        guild_id: GuildId,
    ) -> Option<Option<RoomCodeEvent>> {
        let data_lock = ctx.data.read().await;
        let state_arc = data_lock
            .get::<Self>()
            .expect("Global data for `AmongUsState` must be available")
            .clone();
        drop(data_lock);
        let state_lock = state_arc.read().await;
        state_lock.get_event_code(guild_id).cloned()
    }

    /// Sets the event code for the guild.
    pub fn set_event_code(&mut self, guild_id: GuildId, ev: Option<RoomCodeEvent>) {
        self.codes.insert(guild_id, ev);
    }

    /// Sets the event code for the guild through `Context`.
    pub async fn set_event_code_through_context(
        ctx: &Context,
        guild_id: GuildId,
        ev: Option<RoomCodeEvent>,
    ) {
        let data_lock = ctx.data.read().await;
        let state_arc = data_lock
            .get::<Self>()
            .expect("Global data for `AmongUsState` must be available")
            .clone();
        drop(data_lock);
        let mut state_lock = state_arc.write().await;
        state_lock.set_event_code(guild_id, ev.clone());
    }
}

impl TypeMapKey for AmongUsState {
    type Value = Arc<RwLock<Self>>;
}

/// Room code event for Among Us.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct RoomCodeEvent {
    /// Room code.
    code: RoomCode,
    /// Event datetime.
    datetime: chrono::DateTime<chrono::Utc>,
    /// Message ID.
    msg_id: MessageId,
}

impl RoomCodeEvent {
    /// Creates an event object from the given message.
    #[must_use]
    pub fn try_from_message(msg: &Message) -> Option<Self> {
        let code = RoomCode::new(msg.content.trim())?;
        Some(Self {
            code,
            datetime: msg.timestamp,
            msg_id: msg.id,
        })
    }

    /// Returns the newer event.
    #[must_use]
    fn newer(self, other: Self) -> Self {
        if self.datetime > other.datetime {
            self
        } else {
            other
        }
    }

    /// Returns the room code.
    #[inline]
    #[must_use]
    fn code(&self) -> RoomCode {
        self.code
    }

    /// Returns the datetime of the event.
    #[inline]
    #[must_use]
    fn datetime(&self) -> chrono::DateTime<chrono::Utc> {
        self.datetime
    }
}

/// Room code for Among Us.
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct RoomCode([u8; 6]);

impl RoomCode {
    /// Creates a new room code from the given string.
    pub fn new(s: &str) -> Option<Self> {
        let bytes = s.as_bytes();
        //  * 6 ASCII upper alphanumeric chars.
        //  * At least 1 ASCII alphabet.
        //      * A restriction to reduce false positive for the detection.
        //      * Not sure, but 6 digits are less likely to be a room code.
        if bytes.len() == 6
            && bytes
                .iter()
                .all(|b| b.is_ascii_digit() || b.is_ascii_uppercase())
            && bytes.iter().any(|b| !b.is_ascii_digit())
        {
            let arr = <[u8; 6]>::try_from(bytes)
                .expect("should never fail: already validated to be 6 bytes string");
            Some(Self(arr))
        } else {
            None
        }
    }

    /// Returns the room code as a string slice.
    pub fn as_str(&self) -> &str {
        std::str::from_utf8(&self.0[..]).expect("`RoomCode` type must have 6 ASCII characters")
    }
}

impl fmt::Debug for RoomCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "RoomCode({:?})", self.as_str())
    }
}

impl fmt::Display for RoomCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.as_str())
    }
}

#[group]
#[prefixes("amo", "au", "amous", "amongus", "among-us", "among_us")]
#[description = "Commands for Among Us"]
#[summary = "Help playing Among Us"]
#[commands(map, maps, room)]
pub struct AmongUs;

#[command]
#[description = "Show a map for Among Us"]
#[aliases("m", "ma")]
pub async fn map(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let config = Config::get()?;
    let name_raw = match args.current() {
        Some("all") => {
            // Handle "all" as a special name to show all maps.
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    show_all_maps(m, config);
                    m
                })
                .await?;
            return Ok(());
        }
        Some(v) => v,
        None => {
            // Map name is not passed. Provide recognizable name aliases.
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.add_embed(|e| create_map_name_aliases_embed(e, config));
                    m
                })
                .await?;
            return Ok(());
        }
    };

    let map = match config.maps().get_by_alias(name_raw) {
        Some(v) => v,
        None => {
            // Unknown name is passed. Provide recognizable name aliases.
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.content(format_args!("そんなマップはない: {:?}", name_raw));
                    m.add_embed(|e| create_map_name_aliases_embed(e, config));
                    m
                })
                .await?;
            return Ok(());
        }
    };

    msg.channel_id
        .send_message(&ctx.http, |m| {
            //use serenity::http::AttachmentType;
            //m.content(map.name());
            //m.add_file(AttachmentType::Image(map.image_uri()));
            m.add_embed(|e| {
                e.title(map.name());
                e.image(map.image_uri());
                e
            });
            m
        })
        .await?;

    Ok(())
}

/// Creates an embed to show available map name aliases.
fn create_map_name_aliases_embed<'a>(
    e: &'a mut CreateEmbed,
    config: &'_ Config,
) -> &'a mut CreateEmbed {
    /// Whether to show map aliases inline.
    const INLINE_ALIASES: bool = false;

    e.description("Map name aliases");
    for map in config.maps() {
        e.field(map.name(), map.aliases(), INLINE_ALIASES);
    }
    e
}

#[command]
#[description = "Show all maps for Among Us"]
pub async fn maps(ctx: &Context, msg: &Message) -> CommandResult {
    let config = Config::get()?;

    msg.channel_id
        .send_message(&ctx.http, |m| show_all_maps(m, config))
        .await?;

    Ok(())
}

/// Creates embeds to show all maps.
fn show_all_maps<'a, 'm>(
    m: &'a mut CreateMessage<'m>,
    config: &Config,
) -> &'a mut CreateMessage<'m> {
    for map in config.maps() {
        m.add_embed(|e| {
            e.title(map.name());
            e.image(map.image_uri());
            e
        });
    }
    m
}

#[command]
#[description = "Show the latest Among Us room code"]
#[aliases("r")]
pub async fn room(ctx: &Context, msg: &Message) -> CommandResult {
    let guild_id = msg
        .guild_id
        .ok_or("Guild Id should be known for `among-us room` command")?;

    let event = AmongUsState::get_event_code_through_context(ctx, guild_id).await;

    let event: Option<RoomCodeEvent> = match event {
        Some(v) => v,
        None => {
            // No cache. Fetch recent messages and search for room codes.
            let ev = find_room_code_event(ctx, guild_id).await;
            AmongUsState::set_event_code_through_context(ctx, guild_id, ev.clone()).await;
            ev
        }
    };

    match event {
        Some(event) => {
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.reference_message(msg);
                    let time_delta = TimeDelta::since_now(event.datetime());
                    m.content(format_args!(
                        "`{}` ({})",
                        event.code(),
                        time_delta.display_human_readable()
                    ));
                    m
                })
                .await?;
        }
        None => {
            msg.channel_id
                .send_message(&ctx.http, |m| m.content("No Among Us rooms found"))
                .await?;
        }
    }

    Ok(())
}

/// Finds Among Us room code from the messages history.
async fn find_room_code_event(ctx: &Context, guild_id: GuildId) -> Option<RoomCodeEvent> {
    let mut event: Option<RoomCodeEvent> = None;
    let channels = guild_id.channels(&ctx.http).await.ok()?;
    'channels: for channel_id in channels.keys() {
        let mut msgs = channel_id.messages_iter(&ctx.http).boxed();
        'channel_msgs: while let Some(msg) = msgs.next().await {
            let msg = match msg {
                Ok(v) => v,
                Err(e) => {
                    log::debug!(
                        "Failed to iterate message (guild={:?}, channel={:?}): {}",
                        guild_id,
                        channel_id,
                        e
                    );
                    continue 'channels;
                }
            };

            if let Some(found) = RoomCodeEvent::try_from_message(&msg) {
                if let Some(known) = event {
                    event = Some(RoomCodeEvent::newer(known, found));
                } else {
                    event = Some(found);
                }
                break 'channel_msgs;
            }
        }
    }

    event
}

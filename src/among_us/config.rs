//! Config management for among us.

use std::collections::HashMap;
use std::fmt;
use std::path::Path;

use once_cell::sync::OnceCell;
use serde::Deserialize;

/// Config for Among Us module.
#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    /// Maps.
    #[serde(flatten)]
    maps: Maps,
}

impl Config {
    /// Returns the instance of the static config.
    pub fn get() -> anyhow::Result<&'static Self> {
        /// The lazily loaded instance of the `Config`.
        static CONFIG: OnceCell<Config> = OnceCell::new();

        CONFIG.get_or_try_init(|| {
            log::debug!("Loading among us config");
            let res = Self::from_path("config/among-us.toml");
            match &res {
                Ok(_) => log::debug!("Successfully loaded among us config"),
                Err(e) => log::error!("Failed to load among us config: {}", e),
            }
            res
        })
    }

    /// Loads a config from the given path.
    #[inline]
    fn from_path<P: AsRef<Path>>(path: P) -> anyhow::Result<Self> {
        Self::from_path_impl(path.as_ref())
    }

    /// Loads a config from the given path.
    fn from_path_impl(path: &Path) -> anyhow::Result<Self> {
        let mut data: Self = {
            let s = std::fs::read_to_string(path)?;
            toml::from_str(&s)?
        };
        data.maps.alias_to_map_index = data
            .maps
            .maps
            .iter()
            .enumerate()
            .flat_map(|(index, map)| {
                map.aliases()
                    .iter()
                    .map(move |alias| (alias.to_owned(), index))
            })
            .collect();

        Ok(data)
    }

    /// Returns the maps.
    #[inline]
    #[must_use]
    pub fn maps(&self) -> &Maps {
        &self.maps
    }
}

/// Maps.
#[derive(Debug, Clone, Deserialize)]
pub struct Maps {
    /// Maps.
    maps: Vec<Map>,
    /// Conversion table from name alias to index of `maps`.
    #[serde(skip)]
    alias_to_map_index: HashMap<String, usize>,
}

impl Maps {
    /// Returns a map for the given map name alias, if available.
    #[must_use]
    pub fn get_by_alias(&self, alias: &'_ str) -> Option<&Map> {
        let alias = alias.to_lowercase();
        self.alias_to_map_index.get(&alias).map(|&i| &self.maps[i])
    }

    /// Returns an iterator of the maps.
    #[inline]
    pub fn iter(&self) -> <&Self as IntoIterator>::IntoIter {
        self.into_iter()
    }
}

impl<'a> IntoIterator for &'a Maps {
    type IntoIter = std::slice::Iter<'a, Map>;
    type Item = &'a Map;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        self.maps.iter()
    }
}

/// A map.
#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Map {
    /// Map name.
    name: String,
    /// Map image URI.
    image_uri: String,
    /// Map name aliases.
    #[serde(flatten)]
    aliases: MapNameAliases,
}

impl Map {
    /// Returns the map name.
    #[inline]
    #[must_use]
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Returns the map image URI.
    #[inline]
    #[must_use]
    pub fn image_uri(&self) -> &str {
        &self.image_uri
    }

    /// Returns the map name aliases.
    #[inline]
    #[must_use]
    pub fn aliases(&self) -> &MapNameAliases {
        &self.aliases
    }
}

/// Map name aliases for a map.
#[derive(Debug, Clone, Deserialize)]
pub struct MapNameAliases {
    /// Map name aliases (case insensitive).
    aliases: Vec<String>,
}

impl MapNameAliases {
    /// Returns an iterator of map name aliases.
    #[inline]
    pub fn iter(&self) -> <&Self as IntoIterator>::IntoIter {
        self.into_iter()
    }
}

impl<'a> IntoIterator for &'a MapNameAliases {
    type IntoIter = std::iter::Map<std::slice::Iter<'a, String>, fn(&String) -> &str>;
    type Item = &'a str;

    fn into_iter(self) -> Self::IntoIter {
        self.aliases.iter().map(String::as_str)
    }
}

impl fmt::Display for MapNameAliases {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut iter = self.aliases.iter();
        match iter.next() {
            Some(alias) => f.write_str(alias)?,
            None => return Ok(()),
        }
        iter.try_for_each(|alias| {
            f.write_str(", ")?;
            f.write_str(alias)
        })
    }
}

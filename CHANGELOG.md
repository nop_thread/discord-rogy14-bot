# Change Log

## [Unreleased]

## [0.20211112.0]

* `!14 ver` is added as an alias of `!14 version`.
* Added Among Us map "dleks".

### Added

* `!14 ver` is added as an alias of `!14 version`
* Added Among Us map "dleks".

## [0.20211108.0]

Initial release.

[Unreleased]: <https://gitlab.com/nop_thread/discord-rogy14-bot/-/compare/v0.20211112.0...develop>
[0.20211112.0]: <https://gitlab.com/nop_thread/discord-rogy14-bot/-/tags/v0.20211112.0>
[0.20211108.0]: <https://gitlab.com/nop_thread/discord-rogy14-bot/-/tags/v0.20211108.0>
